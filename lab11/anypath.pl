% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(G,N1,N2,[e(N1,N2)]):-member(e(N1,N2),G).
anypath(G,N1,N2,[e(N1,N3)|L]):- member(e(N1,N3),G),anypath(G,N3,N2,L).
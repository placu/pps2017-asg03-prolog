:- consult('dropFirst.pl').
% equal(+List, +List1)
% true if the two list have the same elements
equal(X,Y):- isSublist(X,Y), isSublist(Y,X).
%isSublist(?List1, +List2)
%true if list1 is a sublist of list2
isSublist([],_):-!.
isSublist([H|T], Y):- member(H,Y), dropFirst(H,Y,Z), isSublist(T,Z).
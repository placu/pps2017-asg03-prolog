% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
:-consult('dropAll.pl').
dropNode(G,N,O):- dropAll(e(N,_),G,G2), dropAll(e(_,N),G2,O).
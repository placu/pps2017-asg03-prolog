% fromCircList(+List,-Graph)
fromCircList([H],[e(H,H)]).
fromCircList([H1,H2|T],[e(H1,H2)|L]):- fromCircList(H1,[H2|T],L).

% fromCircList(+HeadList, +List,-Graph)
fromCircList(H,[H1],[e(H1,H)]):-!.
fromCircList(H,[H1,H2|T],[e(H1,H2)|L]):- fromCircList(H,[H2|T],L).
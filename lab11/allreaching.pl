:- consult('anypath.pl').
:- consult('equal.pl').
% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
allreaching(G,N,L):- findall(X,anypath(G,N,X,_),L1), (var(L) -> L=L1; equal(L,L1)).


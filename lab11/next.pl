% next(@Table,@Player,-Result,-NewTable)
% Table -> [[n,n,n],[x,o,x],[o,x,n]]
% next([[x,n,o],[o,n,x],[x,x,o]],x,R,T).

next(T, P, R, NewT):-
					ground(T),
					ground(P),
					member(P,[o,x]),
					insert(T,P,NewT), 
					result(NewT,R).

insert([R,R1,R2],P,[NewR,R1,R2]):-rowInsert(R,P,NewR).
insert([R,R1,R2],P,[R,NewR1,R2]):-rowInsert(R1,P,NewR1).
insert([R,R1,R2],P,[R,R1,NewR2]):-rowInsert(R2,P,NewR2).

rowInsert([n,Y,Z],P,[P,Y,Z]).
rowInsert([Y,n,Z],P,[Y,P,Z]).
rowInsert([Y,Z,n],P,[Y,Z,P]).

result(T, win(P)):- checkWin(T,P), member(P,[x,o]), !.
result(T, nothinng):- checkNothing(T), !.
result(_, even). 

%check horizontal win
checkWin([[X,X,X]|_],X).
checkWin([_|T],X):- checkWin(T,X).
%check vertical win
checkWin([[X|_],[X|_],[X|_]], X).
checkWin([[_|T],[_|T1],[_|T2]], X):- checkWin([T,T1,T2],X).
%check diagonal win
checkWin([[X|_],[_,X|_],[_,_,X]], X).
checkWin([[_,_,X],[_,X|_],[X|_]], X).

checkNothing([[n|RowT]|T]).
checkNothing([[]|T]):- checkNothing(T).
checkNothing([[_|RowT]|T]):- checkNothing([RowT|T]).
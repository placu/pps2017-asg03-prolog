% game(@Table,@Player,-Result,-TableList)

:-consult('next.pl').
game2(T,P,win(P),[NT]):- next(T,P,win(P),NT).
game2(T,P,even,[NT]):- next(T,P,R,NT), R==even.
game2(T,P,R,[NewT|ListT]):-
					next(T,P,nothing,NewT),
					nextPlayer(P,NP),
					game2(NewT,NP,R,ListT).

nextPlayer(x,o).
nextPlayer(o,x).
% dropAll(?Elem,?List,?OutList)
dropAll(_,[],[]).
dropAll(X,[H|T],L):-copy_term(X,X2),H=X2,!,dropAll(X,T,L).
dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).
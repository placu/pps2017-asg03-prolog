% game(@Table,@Player,-Result,-TableList)

:-consult('next.pl').
game(T,P,NextR,[NewT|ListT]):-
					next(T,P,R,NewT),
					continue(NewT,P,R,NextR,ListT).
					
continue(T,P,nothing,R,NT):- 
					nextPlayer(P, NextP),
					game(T, NextP, R, NT).
continue(T,P,R,R,[]):- R\==nothing.

nextPlayer(x,o).
nextPlayer(o,x).
% sublist(List1,List2)
% List1 should be a subset of List2
% example: subList([1,2],[5,3,2,1]).
:- consult('search.pl').
subList([H],L2):-search(H,L2).
subList([H|T],L2):-search(H,L2), subList(T,L2).
% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
:-consult('last.pl').
seqR2(0,[0]).
seqR2(N,L):- N1 is N-1, seqR2(N1,L1), last(L1,N,L).
% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[N|T]):- N1 is N-1, seqR(N1,T). 
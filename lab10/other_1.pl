% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
:-consult('last.pl').
inv([],[]).
inv([H|T], Inv):- inv(T,L1), last(L1,H,Inv).
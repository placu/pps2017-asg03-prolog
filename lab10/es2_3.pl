% sum(List,Sum)
sum([],0).
sum([H|T], X):- sum(T, Y), X is Y+H.
% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element

max([H], H).
max([H|T], Max) :- max(T, MaxT), max(H, MaxT, Max),!.
max(X,Y,X) :- X >= Y.
max(X,Y,Y) :- X < Y.
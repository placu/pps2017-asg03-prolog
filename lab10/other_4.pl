% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).

proj([],[]).
proj([[H|_]|T], [H|L]):-proj(T,L).
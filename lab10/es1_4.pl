% search_anytwo(Elem,List)
% looks for any Elem that occurs two times
:- consult('search.pl').
search_anytwo(X,[X|T]):- search(X,T).
search_anytwo(X,[_|T]):- search_anytwo(X,T).
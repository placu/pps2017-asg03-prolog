% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).

times(L,0,[]).
times(L,N,L1):- N1 is N-1, times(L,N1,L2), append(L,L2,L1).
%last(+List, +NewTail, -OutList)
%append NewTail to the end of List
last([],T,[T]).
last([H|T],NewT,[H|L1]):- last(T,NewT,L1).